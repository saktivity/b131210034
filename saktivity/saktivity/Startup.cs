﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(saktivity.Startup))]
namespace saktivity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
